package com.hendisantika.belajargooglesso.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by IntelliJ IDEA.
 * Project : belajar-google-sso
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/11/18
 * Time: 22.39
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/")
public class HomeController {

    @GetMapping("home")
    public void home() {
    }

    @GetMapping("auth")
    @ResponseBody
    public Authentication auth(Authentication auth) {
        return auth;
    }

    @PreAuthorize("hasAuthority('VIEW_REKENING')")
    @GetMapping("rekening")
    public String daftarRekening() {
        return "rekening";
    }

    @PreAuthorize("hasAuthority('VIEW_MUTASI')")
    @GetMapping("mutasi")
    public String mutasiRekening() {
        return "mutasi";
    }

    @PreAuthorize("hasAuthority('EDIT_TRANSFER')")
    @GetMapping("transfer")
    public String transfer() {
        return "transfer";
    }

}