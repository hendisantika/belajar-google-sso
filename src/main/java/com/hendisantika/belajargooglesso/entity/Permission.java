package com.hendisantika.belajargooglesso.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * Project : belajar-google-sso
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/11/18
 * Time: 22.34
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "s_permission")
@Data
public class Permission {
    @Id
    private String id;

    @Column(name = "permission_label")
    private String label;

    @Column(name = "permission_value")
    private String value;
}